package com.example.ud6_ejemplo5;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

public class Dialogo extends DialogFragment {

    // La actividad que crea una instancia de este dialogo debe implementar esta interfaz.
    public interface DialogoListener {
        public void onDialogoAceptarClick(Dialogo dialogo);
        public void onDialogoCancelarClick(Dialogo dialogo);
    }

    // Usamos esta instancia para los eventos
    private DialogoListener listener;

    // Texto a pasar a la actividad
    private String texto;

    public String getTexto() {
        return texto;
    }

    // Sobreescribimos el método onAttach() para instanciar el DialogoListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Verificamos que la actividad que abre el dialogo implementa la interfaz
        try {
            // Instanciamos el DialogoListener para enviar los eventos a la actividad
            listener = (DialogoListener) context;
        } catch (ClassCastException e) {
            // Si la actividad no implementa la interfaz lanzamos una excepción
            throw new ClassCastException(getActivity().toString() + " must implement NoticeDialogListener");
        }
    }

    // Creamos el dialogo
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Usamos la clase Builder para la construcción del Dialogo
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Inflamos el layout del dialogo
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_dialogo, null);

        // Asignamos la vista con el layout del dialogo y añadimos las acciones a los botones
        builder.setView(view)
                .setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Obtenemos el texto escrito en el EditText y lo almacenamos en el atributo texto
                        EditText editText = view.findViewById(R.id.editText);
                        texto = editText.getText().toString();

                        listener.onDialogoAceptarClick(Dialogo.this);
                    }
                })
                .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onDialogoCancelarClick(Dialogo.this);
                    }
                });

        return builder.create();
    }
}
